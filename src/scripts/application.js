var XOModule = angular.module('XO', []);
var url = 'http://127.0.0.1:1337';
var socket = io.connect(url);

/**
 * Data model version 0.1:
 * {
 *   user: { uid, name, role, turn, sign },
 *   users: {
 *     uid1: { uid, name, role }
 *     uid2: { uid, name, role }
 *   },
 *   dimensions: { x, y },
 *   board: [['','',''],['','',''],['','','']]
 * }
 */

/**
 * Data model version 0.2:
 * {
 *   user: { uid, name, role },
 *   users: {
 *     uid1: { uid, name, role }
 *     uid2: { uid, name, role }
 *   },
 *   game: {
 *     gid,
 *     dimensions: { x, y },
 *     board: [['','',''],['','',''],['','','']],
 *     turn: true || false,
 *     sign: '' || 'X' || 'O',
 *     host: { uid, name, role }
 *     guest: { uid, name, role }
 *   },
 *   games: {
 *     gid1: { gid, dimensions, board, host, guest },
 *     gid2: { gid, dimensions, board, host, guest }
 *   }
 * }
 */

/**
 * Data model version 0.3:
 * {
 *   url: 'http://127.0.0.1:1337',
 *   user: { uid, name },
 *   users: {
 *     uid1: { uid, name, role }
 *     uid2: { uid, name, role }
 *   },
 *   game: {
 *     gid: '...',
 *     host: '...',
 *     guest: '...',
 *     board: [['','',''],['','',''],['','','']],
 *     turn: true || false,
 *     sign: '' || 'X' || 'O'
 *   },
 *   games: {
 *     gid1: { gid, host, guest },
 *     gid2: { gid, host, guest }
 *   }
 * }
 */

/**
 * Controller for our game.
 * 
 * @param {Object} $scope Scope our our models
 */
function XOController($scope) {
  
  // Myself
  $scope.user = {};

  // All users
  $scope.users = {};

  // Current game (only yours)
  $scope.game = {};

  // All games
  $scope.games = {};

  // Internal elements
  $scope.internal = {};

  // Server url
  $scope.url = 'http://127.0.0.1:1337';

  function keys(obj) {
    var arr = [];
    for (var k in obj) {
      if (obj.hasOwnProperty(k)) {
        arr.push(k);
      }
    }
    return arr;
  }

  function sample(obj) {
    var values = keys(obj);
    var length = values.length;
    var randomKey = Math.floor((Math.random() * length));

    return values[randomKey];
  }

  /**
   * Initializes the game, gets add data from server about
   * current user and current game.
   * 
   * @method initGame
   */
  $scope.initGame = function () {
    if (localStorage.uid) {
      socket.emit('browser.ready', { uid: localStorage.uid });
    }
  };

  /**
   * Adds new user to scope.
   * 
   * @method addUser
   */
  $scope.addUser = function () {
    socket.emit('user.register', { name: $scope.user.name });
  };

  /**
   * Start the game
   * 
   * @method startGame
   */
  $scope.startGame = function () {
    socket.emit('game.start', { uid: $scope.user.uid });
  };

  /**
   * Join the game
   * 
   * @method joinGame
   */
  $scope.joinGame = function () {
    socket.emit('game.join', { gid: sample($scope.games), uid: $scope.user.uid });
  };

  /**
   * Reset the game
   * 
   * @method resetGame
   */
  $scope.resetGame = function () {
    socket.emit('game.reset', { gid: $scope.game.gid });
  };

  /**
   * Click on board
   *
   * @method click
   * @param  {Object} $event
   */
  $scope.click = function($event) {
    var x, y, $element = angular.element($event.target);

    // verify if it is your turn, if clicked on cell and if cell is empty
    if ($scope.game.turn === $scope.user.uid && $element.hasClass('cell') && $element.hasClass('empty')) {
      x = $element.data('row');
      y = $element.data('column');
      $scope.game.board[x][y].value = ($scope.game.host === $scope.user.uid) ? 'X' : 'O';
      $scope.game.turn = '';
      socket.emit('game.mark', { gid: $scope.game.gid, uid: $scope.user.uid, x: x, y: y });
    }
  };

  //
  // Listeners
  // ------------------------------------------------------

  socket.on('browser.ready.done', function(scope) {
    if ( ! scope.user) {
      delete localStorage.uid;
    }

    $scope.user = scope.user;
    $scope.users = scope.users;
    $scope.game = scope.game;
    $scope.games = scope.games;
    $scope.$apply();
  });

  socket.on('user.register.done', function(user) {
    localStorage.uid = user.uid;
    socket.emit('browser.ready', { uid: user.uid });
  });

  socket.on('user.register.done.global', function(user) {
    $scope.users[user.uid] = user;
    $scope.$apply();
  });

  socket.on('user.login.global', function(user) {
    $scope.users[user.uid] = user;
    $scope.$apply();
  });

  socket.on('user.logout.global', function(user) {
    delete $scope.users[user.uid];
    $scope.$apply();
  });

  socket.on('game.start.done', function(game) {
    $scope.game = game;
    $scope.$apply();
  });

  socket.on('game.start.done.global', function(game) {
    $scope.games[game.gid] = game;
    $scope.users[game.host].role = 'host';
    $scope.$apply();
  });

  socket.on('game.join.done', function(game) {
    $scope.game = game;
    $scope.$apply();
  });

  socket.on('game.join.done.global', function(game) {
    $scope.games[game.gid] = game;
    $scope.users[game.guest].role = 'guest';
    if ($scope.game.gid === game.gid) {
      $scope.game.guest = game.guest;
    }
    $scope.$apply();
  });

  socket.on('game.reset.done.global', function(game) {
    delete $scope.games[game.gid];

    // Reset host user
    $scope.users[game.host].role = 'visitor';
    if (game.host === $scope.user.uid) {
      $scope.game = {};
    }

    // Reset guest user
    if (game.guest) {
      $scope.users[game.guest].role = 'visitor';
      if (game.guest === $scope.user.uid) {
        $scope.game = {};
      }
    }

    $scope.$apply();
  });

  socket.on('game.mark.done.global', function(game, result) {

    if ($scope.game.gid === game.gid) {
      if ($scope.game.turn !== '') {
        $scope.game.board[game.x][game.y].value = game.sign;
        $scope.internal.last = { x: game.x, y: game.y };
      }
      $scope.game.turn = game.turn;

      if (result) {
        $scope.game.result = result;
        $scope.game.turn = '';
        $scope.game.message = 'You ' + ($scope.user.uid === result.winner ? 'win' : 'lost');

        for (i in result.positions) {
          $scope.game.board[result.positions[i].row][result.positions[i].column].win = true;
        }
      }

      $scope.$apply();
    }
  });

}