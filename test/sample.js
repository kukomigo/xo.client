describe("Sample tests", function () {
  "use strict";
 
  it("should gives the correct sum result", function () {
    // Arrange
    var num1 = 1;
    var num2 = 3;
    var expected = 4;

    // Act
    var result = num1 + num2;

    // Assert
    expect(result).toBe(expected);
  });

});
