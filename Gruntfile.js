module.exports = function (grunt) {
  'use strict';

  // Load all npm dependencies
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),


    //
    // Configure projects validators
    // -----------------------------------------

    jshint: {
      dev: {
        options: {
          globals: {
            "console": true,
            "$": true
          }
        },
        src: ['src/scripts/**/*.js']
      }
    },

    lesslint: {
      source: {
        options: {
          csslint: {
            "known-properties": false
          }
        },
        src: ['src/less/*.less']
      }
    },


    //
    // Configure watching project's development
    // -----------------------------------------

    express: {
      development: {
        options: {
          port: 9000,
          hostname: 'localhost',
          bases: ['src'],
          livereload: true
        }
      }
    },

    open: {
      development: {
        url: 'http://localhost:<%= express.development.options.port %>'
      }
    },

    watch: {
      less: {
        files: 'src/less/*.less',
        tasks: ['less']
      },
      all: {
        files: ['src/scripts/**/*.js', 'src/styles/*.css', 'src/**/*.html'],
        options: {
          livereload: true
        }
      }
    },


    //
    // Configure installing project's files
    // -----------------------------------------

    bower: {
      development: {
        options: {
          targetDir: 'src/bower',
          cleanBowerDir: true,
          layout: 'byComponent'
        }
      }
    },

    clean: {
      development: ['src/bower', 'src/styles'],
      production: ['dist']
    },


    //
    // Configure project's development flow
    // -----------------------------------------

    less: {
      development: {
        options: {
          paths: ['src/less']
        },
        files: {
          'src/styles/application.css': 'src/less/application.less'
        }
      }
    },

    karma: {
      development: {
        options: {
          files: ['src/**/*.js' ,'test/**/*.js'],
          singleRun: true,
          frameworks: ['jasmine'],
          browsers: ['PhantomJS']
        }
      }
    }

    //
    // Configure building project's files
    // -----------------------------------------

  });


  //
  // Register all important tasks
  // -----------------------------------------

  // I'm pretty out of habit to use this
  grunt.registerTask('lint', ['jshint', 'lesslint']);

  // Compile less and coffee scripts
  grunt.registerTask('prepare', ['clean:development' ,'bower', 'less']);

  // Open server and listen to all changes
  grunt.registerTask('run', ['prepare', 'lint', 'express', 'open', 'watch']);

  // Run tests
  grunt.registerTask('test', ['lint' ,'prepare', 'karma']);

};
